package services

import (
	"errors"
	"github.com/DevAgani/note-app-backend/internal/models"
	"github.com/DevAgani/note-app-backend/internal/store"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type UserInterface interface {
	Register(user models.User) (primitive.ObjectID,error)
	Update(filter, update bson.M) error
	CheckPresence(filter bson.M) (bool,error)
	Delete(filter bson.M) (int64,error)
	FindOne(filter bson.M) (models.User,error)
	UpdatePassword(filter,update bson.M) error
	FetchUser(filter bson.M) (models.User,error)
	FetchUserNotes(filter bson.M)(interface{},error)
}

type User struct {
	Store store.StoreInterface
}

func (usr *User) Register(user models.User) (primitive.ObjectID,error)  {
	timestamp := time.Now()
	user.DateCreated = timestamp
	user.DateUpdated = timestamp

	if !user.Validate(){
		return primitive.NilObjectID,errors.New("the provided user data is invalid")
	}
	hashedPass, err := bcrypt.GenerateFromPassword([]byte(user.Password),12)
	if err != nil{
		return primitive.NilObjectID, err
	}
	user.Password = string(hashedPass)
	// TODO: instead send the user an email to validate his account.
	user.IsVerified = true

	return usr.Store.User().InsertOne(user)
}
func (usr *User) Update(filter,update bson.M) error{
	update["date_updated"] = time.Now()
	return usr.Store.User().FindOneAndUpdate(filter,bson.M{"$set":update})
}
func (usr *User) CheckPresence(filter bson.M) (bool,error)  {
	return usr.Store.User().CheckPresence(filter)
}
func (usr *User) Delete(filter bson.M) (int64,error)  {
	return usr.Store.User().DeleteOne(filter)
}
func (usr *User) FindOne(filter bson.M) (models.User,error){
	return usr.Store.User().FindOne(filter)
}
func (usr *User) UpdatePassword(filter,update bson.M) error  {
	newPass, ok := update["password"]
	if ok {
		hashedPass, err := bcrypt.GenerateFromPassword([]byte(newPass.(string)),12)
		if nil != err {
			return err
		}
		update["password"] = hashedPass
	}
	update["date_updated"] = time.Now()
	err := usr.Store.User().FindOneAndUpdate(filter,bson.M{"$set":update})
	if nil != err {
		return err
	}
	return nil
}
func (usr *User) FetchUser(filter bson.M) (models.User,error)  {
	return usr.Store.User().FindOne(filter)
}
func (usr *User) FetchUserNotes(filter bson.M)(interface{},error){
return nil,nil
}


func GetUserService(store store.StoreInterface) User{
	return User{Store: store}
}