package services

import (
	"errors"
	"github.com/DevAgani/note-app-backend/internal/models"
	"github.com/DevAgani/note-app-backend/internal/store"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type NoteInterface interface {
	Create(note models.Note) (primitive.ObjectID,error)
	Update(filter bson.M,update bson.M) error
	Delete(filter bson.M) (int64,error)
	Fetch(filter bson.M) (interface{},error)
	FetchAllNotes(filter bson.M) (interface{},error)
}

type Note struct {
	Store store.StoreInterface
}

func( nt * Note) Create(note models.Note) (primitive.ObjectID,error) {
	timestamp := time.Now()
	note.DateUpdated = timestamp
	note.DateCreated = timestamp
	if !note.IsValid(){
		return primitive.NilObjectID, errors.New("information is incomplete")
	}
	res, err := nt.Store.Note().InsertOne(note)
	return res,err
}
func( nt * Note) Update(filter bson.M,update bson.M) error {
	timestamp := time.Now()
	update["date_updated"] = timestamp
	return nt.Store.Note().FindOneAndUpdate(filter,bson.M{"$set":update})
}
func( nt * Note) Delete(filter bson.M) (int64,error) {
	return nt.Store.Note().DeleteOne(filter)
}
func (nt *Note)Fetch(filter bson.M) (interface{},error)  {
	return nt.Store.Note().FindOne(filter)
}
func (nt *Note) FetchAllNotes(filter bson.M)(interface{},error)  {
	notes, err := nt.Store.Note().FetchAll(filter)
	if nil != err {
			return nil, err
		}
	return notes, nil
}

func GetNotesService(store store.StoreInterface) Note{
	return Note{Store: store}
}