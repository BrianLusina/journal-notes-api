package controllers

import (
	"encoding/json"
	"github.com/DevAgani/note-app-backend/internal/models"
	"github.com/DevAgani/note-app-backend/internal/services"
	"github.com/DevAgani/note-app-backend/internal/store"
	"github.com/DevAgani/note-app-backend/internal/utils"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)
const (
	EmailRegex = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
)

type UserControllers struct {
	UserService services.UserInterface
 }

func (usr *UserControllers) Register(c *gin.Context){
	var user models.User

	err := json.NewDecoder(c.Request.Body).Decode(&user)
	if nil != err {
		utils.RespondWithError(c, http.StatusInternalServerError,err.Error())
		return
	}
	//check the required fields
	//Email, FirstName, LastName,Password
	if user.Email == ""{
		utils.RespondWithError(c, http.StatusInternalServerError,"email doesn't meet required criteria")
		return
	}
	if user.FirstName == ""{
		utils.RespondWithError(c, http.StatusInternalServerError,"first name is required")
		return
	}
	if user.LastName == ""{
		utils.RespondWithError(c, http.StatusInternalServerError,"last name is required")
		return
	}
	if user.Password == "" || len(user.Password) < 5{
		utils.RespondWithError(c, http.StatusInternalServerError,"password doesn't meet required criteria")
		return
	}
	//check if a similar user exists
	userIsNotUnique, err := usr.UserService.CheckPresence(bson.M{"email":user.Email})
	if nil != err{
		utils.RespondWithError(c, http.StatusInternalServerError,err.Error())
		return
	}
	if userIsNotUnique{
		utils.RespondWithError(c, http.StatusInternalServerError,"email already taken")
		return
	}

	//call user service to persist
	id, err := usr.UserService.Register(user)
	if nil != err {
		utils.RespondWithError(c, http.StatusInternalServerError,err.Error())
		return
	}

	utils.RespondWithJSON(c,id)
}
func (usr *UserControllers) Login(c *gin.Context)  {
	var user models.User
	err := json.NewDecoder(c.Request.Body).Decode(&user)
	if nil != err {
		utils.RespondWithError(c, http.StatusInternalServerError,"provide email and password")
		return
	}
	if user.Email == ""{
		utils.RespondWithError(c, http.StatusInternalServerError,"email is required")
		return
	}
	if user.Password == "" || len(user.Password) < 5{
		utils.RespondWithError(c, http.StatusInternalServerError,"password doesn't meet requirements")
		return
	}
	//Fetch User information
	savedUser, err := usr.UserService.FetchUser(bson.M{"email":user.Email})
	if nil != err {
		utils.RespondWithError(c, http.StatusInternalServerError,err.Error())
		return
	}
	validPassword := savedUser.ValidatePassword(user.Password)
	if !validPassword{
		utils.RespondWithError(c, http.StatusInternalServerError,"the credentials provided don't match")
		return
	}
	// Create a JWT for a successfully authenticated user
	expTime, webToken, err := utils.GenerateJWT(savedUser.ID.Hex())
	if nil != err{
		utils.RespondWithError(c, http.StatusInternalServerError,err.Error())
		return
	}
	// fetch notes if at all there are any
	//notes, err := usr.UserService.
	savedUser.Password = ""
	authResponse := models.AuthResponse{
		User : savedUser,
		JWT: webToken,
		ExpiryTime: expTime,
	}

	utils.RespondWithJSON(c, authResponse)
}
func (usr *UserControllers) UpdatePassword(c *gin.Context)  {
	// Update password
	var user models.User
	err := json.NewDecoder(c.Request.Body).Decode(&user)
	if nil != err {
		utils.RespondWithError(c,http.StatusInternalServerError,"malfunctioned request")
		return
	}
	if user.Password == "" || len(user.Password) <5 {
		utils.RespondWithError(c,http.StatusInternalServerError,"provided details don't meet criteria")
		return
	}

	id := c.Param("id")
	userID ,err := primitive.ObjectIDFromHex(id)
	if nil != err {
		utils.RespondWithError(c,http.StatusInternalServerError, "invalid user details")
		return
	}
	count, err := usr.UserService.FindOne(bson.M{"_id":userID})
	if nil != err {
		utils.RespondWithError(c,http.StatusInternalServerError,err.Error())
		return
	}
	if count.ID != userID {
		utils.RespondWithError(c,http.StatusInternalServerError,"sorry the user doesn't exist")
		return
	}
	filter := bson.M{"_id":userID}
	update := bson.M{"password":user.Password}

	err = usr.UserService.UpdatePassword(filter,update)
	if nil != err {
		utils.RespondWithError(c, http.StatusInternalServerError,err.Error())
		return
	}

	utils.RespondWithSuccess(c)

}
func (usr *UserControllers) Delete(c *gin.Context)  {
	id := c.Param("id")
	userID ,err := primitive.ObjectIDFromHex(id)
	if nil != err {
		utils.RespondWithError(c,http.StatusInternalServerError, "invalid user details")
		return
	}
	count, err := usr.UserService.FindOne(bson.M{"_id":userID})
	if nil != err {
		utils.RespondWithError(c,http.StatusInternalServerError,err.Error())
		return
	}
	if count.ID != userID {
		utils.RespondWithError(c,http.StatusInternalServerError,"sorry the user you want to remove doesn't exist")
		return
	}
	filter := bson.M{"_id":userID}
	// Also delete the notes attached to a user, since the user
	deletedCount, err := usr.UserService.Delete(filter)
	if nil != err {
		utils.RespondWithError(c, http.StatusInternalServerError,err.Error())
		return
	}
	if deletedCount == 0{
		utils.RespondWithError(c, http.StatusInternalServerError,"something happened that i can't explain")
		return
	}

	utils.RespondWithSuccess(c)
}
/**
	1. Update the list of categories, i.e if a user create a new category
	2. Update FirstName and LastName (If they wish to update this, in the future
		it'll logical to allow one update for the names for a period of three months,
		It's just a nice way of enforcing multiple name updates)
	3.
 */
func (usr *UserControllers) Update(c *gin.Context){
	var user models.User
	err := json.NewDecoder(c.Request.Body).Decode(&user)
	if nil != err {
		utils.RespondWithError(c, http.StatusInternalServerError,err.Error())
		return
	}
	//get the user unique id
	id := c.Param("id")
	userID ,err := primitive.ObjectIDFromHex(id)

	if nil != err {
		utils.RespondWithError(c, http.StatusInternalServerError,err.Error())
		return
	}
	filter := bson.M{"_id":userID}
	count, err := usr.UserService.FindOne(filter)
	if nil != err {
		utils.RespondWithError(c,http.StatusInternalServerError,err.Error())
		return
	}
	if count.ID != userID {
		utils.RespondWithError(c,http.StatusInternalServerError,"sorry the user doesn't exist")
		return
	}
	var update = bson.M{}

	if user.FirstName != ""{
		update["first_name"] = user.FirstName
	}
	if user.LastName != ""{
		update["last_name"] = user.LastName
	}
	if len(update) > 0{
		err = usr.UserService.Update(filter,update)
		if nil != err {
			utils.RespondWithError(c, http.StatusInternalServerError,err.Error())
			return
		}
		utils.RespondWithSuccess(c)
	}else{
		utils.RespondWithError(c,http.StatusInternalServerError,"please enter the information you want to update")
		return
	}






}
func GetUserController(store store.StoreInterface) UserControllers  {
	userService := services.GetUserService(store)
	return UserControllers{
		UserService: &userService,
	}
}
