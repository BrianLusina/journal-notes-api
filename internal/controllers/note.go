package controllers

import (
	"encoding/json"
	"github.com/DevAgani/note-app-backend/internal/models"
	"github.com/DevAgani/note-app-backend/internal/services"
	"github.com/DevAgani/note-app-backend/internal/store"
	"github.com/DevAgani/note-app-backend/internal/utils"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

type NotesControllers struct {
	NotesService services.NoteInterface
}

func (nc *NotesControllers) Get(c *gin.Context)  {
	//Fetch all notes
	user, ok := c.Get("user")
	if !ok{
		utils.RespondWithError(c, http.StatusInternalServerError,"you're not authorized to perform this action")
		return
	}
	id := user.(models.User).ID
	notes, err := nc.NotesService.FetchAllNotes(bson.M{"user_id":id})
	if nil != err {
		utils.RespondWithError(c,http.StatusInternalServerError,err.Error())
		return
	}

	utils.RespondWithJSON(c, notes)
}
func (nc *NotesControllers) Create(c *gin.Context)  {
	// since a note is attached to a particular user,
	// the payload to create a user must/has user info
	// Create a note

	userInfo,ok := c.Get("user")
	if !ok{
		utils.RespondWithError(c, http.StatusInternalServerError,"you're not authorized to perform this action")
		return
	}
	var note models.Note

	err := json.NewDecoder(c.Request.Body).Decode(&note)
	if nil != err{
		utils.RespondWithError(c, http.StatusInternalServerError,err.Error())
		return
	}
	if !note.IsValid(){
		utils.RespondWithError(c,http.StatusInternalServerError,"please provide all required fields")
		return
	}
	note.UserID = userInfo.(models.User).ID
	savedNote, err := nc.NotesService.Create(note)
	if err != nil{
		utils.RespondWithError(c,http.StatusInternalServerError,err.Error())
		return
	}
	utils.RespondWithJSON(c, savedNote)
}
func (nc *NotesControllers) Update(c *gin.Context)  {
	// Update the note and add the initial content to the history object
	user, ok := c.Get("user")
	if !ok{
		utils.RespondWithError(c, http.StatusInternalServerError,"you're not authorized to perform this action")
		return
	}
	//fetch the note id
	paramNoteId := c.Param("id")
	noteId, err := primitive.ObjectIDFromHex(paramNoteId)
	if nil != err{
		utils.RespondWithError(c,http.StatusBadRequest,err.Error())
		return
	}
	//userId := user.(models.User).ID

	var note models.Note
	err = json.NewDecoder(c.Request.Body).Decode(&note)
	if nil != err {
		utils.RespondWithError(c,http.StatusInternalServerError,"please provide all required fields")
		return
	}
	//confirm whether the user editing is the owner
	noteOwner, err := nc.NotesService.Fetch(bson.M{"_id":noteId})
	if nil != err{
		utils.RespondWithError(c, http.StatusInternalServerError,err.Error())
		return
	}
	if noteOwner.(models.Note).UserID != user.(models.User).ID{
		utils.RespondWithError(c, http.StatusUnauthorized,err.Error())
		return
	}
	//check the fields that have been updated
	filter := bson.M{"_id":noteId}
	update := bson.M{}
	if note.Title != ""{
		update["title"] = note.Title
	}
	if note.Category != ""{
		update["category"] = note.Category
	}
	if len(note.Tags) > 0 {
		update["tags"] = note.Tags
	}
	if note.Content != ""{
		update["content"] = note.Content
	}
	if len(update) == 0 {
		utils.RespondWithError(c,http.StatusInternalServerError,"please provide information to update")
		return
	}
	err = nc.NotesService.Update(filter,update)
	if nil != err {
		utils.RespondWithError(c,http.StatusInternalServerError,err.Error())
		return
	}

	utils.RespondWithSuccess(c)
}
func (nc *NotesControllers) Delete(c *gin.Context)  {
	// Delete a note.
	user,ok := c.Get("user")
	if !ok{
		utils.RespondWithError(c, http.StatusInternalServerError,"something went wrong")
		return
	}
	id := c.Param("id")
	noteId, err := primitive.ObjectIDFromHex(id)
	if nil != err{
		utils.RespondWithError(c,http.StatusInternalServerError,err.Error())
		return
	}
	_,err = nc.NotesService.Delete(bson.M{"_id":noteId,"user_id":user.(models.User).ID})
	if nil != err {
		utils.RespondWithError(c,http.StatusInternalServerError,"could not delete")
		return
	}

	utils.RespondWithSuccess(c)
}
func (nc *NotesControllers) DeleteAll(c *gin.Context)  {
	// Delete all notes
	// Two options - 1. Actual deletion
	// 			   - 2. Set a Delete Flag.
}
func GetNotesController(store store.StoreInterface) NotesControllers  {
	noteService := services.GetNotesService(store)
	return NotesControllers{
		NotesService: &noteService,
	}
}