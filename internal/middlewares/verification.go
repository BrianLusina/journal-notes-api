package middlewares

import (
	"errors"
	"github.com/DevAgani/note-app-backend/internal/keys"
	"github.com/DevAgani/note-app-backend/internal/services"
	"github.com/DevAgani/note-app-backend/internal/store"
	"github.com/DevAgani/note-app-backend/internal/utils"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"strings"
)

func getBearerToken(header string) (string,error)  {
	if header == ""{
		return "", errors.New("authorization header is required")
	}
	token := strings.Split(header,".")
	if len(token) != 3{
		return "", errors.New("malformed bearer token")
	}
	return header ,nil
}


// VerifyJSONWebToken checks whether a valid JSON Web Token is in the headers
func VerifyJSONWebToken(storeInterface store.StoreInterface)  func( *gin.Context){
	return func(context *gin.Context) {
		// ParseJWT from Authorization header
		authHeader := context.GetHeader("Authorization")

		JSONWebToken, err := getBearerToken(authHeader)
		if nil != err {
			utils.RespondWithError(context,http.StatusUnauthorized,err.Error())
			context.Abort()
			return
		}

		// Token Validation
		token, err := jwt.Parse(JSONWebToken,func(token *jwt.Token) (interface{},error){
			_, ok := token.Method.(*jwt.SigningMethodHMAC)
			if !ok{
				return nil, errors.New("")
			}
			return []byte(keys.GetEnvironmentVariables().Secret),nil
		})
		if nil != err || !token.Valid {
			utils.RespondWithError(context,http.StatusUnauthorized,err.Error())
			context.Abort()
			return
		}
		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok{
			utils.RespondWithError(context,http.StatusUnauthorized,"invalid authorizations")
			context.Abort()
			return
		}
		userID, err := primitive.ObjectIDFromHex(claims["id"].(string))
		if nil != err{
			utils.RespondWithError(context, http.StatusUnauthorized,err.Error())
			context.Abort()
			return
		}
		usr := services.GetUserService(storeInterface)
		userInfo, err := usr.FindOne(bson.M{"_id":userID})
		if nil != err{
			utils.RespondWithError(context,http.StatusUnauthorized,err.Error())
			context.Abort()
			return
		}
		context.Set("user",userInfo)
		context.Next()
	}
}
