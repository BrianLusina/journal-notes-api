package routes

import (
	"github.com/DevAgani/note-app-backend/internal/store"
	"github.com/gin-gonic/gin"
)

func createSubrouter(prefix string, router *gin.Engine) *gin.RouterGroup {
	return router.Group(prefix)
}

// InitRoutes attaches all routes to the router
func InitRoutes(router *gin.Engine,store store.StoreInterface)  {
	userRoutes(createSubrouter("/user",router),store)
	notesRoutes(createSubrouter("/note",router),store)
}