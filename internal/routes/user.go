package routes

import (
	"github.com/DevAgani/note-app-backend/internal/controllers"
	"github.com/DevAgani/note-app-backend/internal/store"
	"github.com/gin-gonic/gin"
)

func userRoutes(group *gin.RouterGroup,storeInterface store.StoreInterface){
	//group.Use(middlewares.VerifyJSONWebToken(storeInterface))
	controller := controllers.GetUserController(storeInterface)
	group.POST("/login",controller.Login)
	group.POST("/register",controller.Register)
	group.PUT("/:id/pass",controller.UpdatePassword)
	group.PUT("/:id",controller.Update)
	group.DELETE("/:id", controller.Delete)
}
