package routes

import (
	"github.com/DevAgani/note-app-backend/internal/controllers"
	"github.com/DevAgani/note-app-backend/internal/middlewares"
	"github.com/DevAgani/note-app-backend/internal/store"
	"github.com/gin-gonic/gin"
)

func notesRoutes(group *gin.RouterGroup,storeInterface store.StoreInterface){
	group.Use(middlewares.VerifyJSONWebToken(storeInterface))
	controller := controllers.GetNotesController(storeInterface)
	group.GET("/fetch",controller.Get)
	group.POST("/create",controller.Create)
	group.PUT("/:id/edit",controller.Update)
	group.DELETE("/:id", controller.Delete)
}
