package server

import (
	"context"
	"fmt"
	"github.com/DevAgani/note-app-backend/internal/keys"
	"github.com/DevAgani/note-app-backend/internal/models"
	"github.com/DevAgani/note-app-backend/internal/routes"
	"github.com/DevAgani/note-app-backend/internal/store"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type instance struct {
	Keys models.Keys
	Server *gin.Engine
}

// Start runs the server.
func (s *instance) Start()  {
	storeInstance := store.GetStore()
	storeInstance.Connect()
	defer storeInstance.Disconnect()

	s.Keys = keys.GetEnvironmentVariables()
	s.Server = gin.Default()

	routes.InitRoutes(s.Server,storeInstance)

	port := fmt.Sprintf(":%s",s.Keys.Port)

	svr := &http.Server{
		Addr: port,
		Handler: s.Server,
	}
	go func() {
		err := svr.ListenAndServe()
		if nil != err && err != http.ErrServerClosed{
			log.Fatal("failed to start server with error: ",err.Error())
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	log.Println("Shutting down server")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err := svr.Shutdown(ctx)
	if err != nil {
		log.Fatal("failed to shut down server with error: ", err.Error())
	}

	log.Println("Server shut down gracefully")

}

// CreateInstance creates a new server instance.
func CreateInstance() *instance {
	return &instance{}
}
