package store

import (
	"context"
	"errors"
	"github.com/DevAgani/note-app-backend/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type noteInterface interface {
	InsertOne(note models.Note) (primitive.ObjectID,error)
	FindOne(filter bson.M) (models.Note,error)
	FindOneAndUpdate(filter bson.M, update bson.M) error
	DeleteOne(bson.M) (int64, error)
	DeleteMany(bson.M) (int64, error)
	FetchAll(filter bson.M) ([]models.Note,error)
}

type note struct {
	db *mongo.Database
}

func (n *note)InsertOne(note models.Note) (primitive.ObjectID,error){
	col := n.db.Collection(NotesCollection)
	res, err := col.InsertOne(context.TODO(),note)
	if nil != err {
		return primitive.NilObjectID,errors.New("could not save user")
	}
	return res.InsertedID.(primitive.ObjectID),nil

}
func (n *note)FindOne(filter bson.M) (models.Note,error){
	var note models.Note
	col := n.db.Collection(NotesCollection)

	queryRes := col.FindOne(context.TODO(),filter)
	if queryRes.Err() != nil{
		return models.Note{}, queryRes.Err()
	}
	err := queryRes.Decode(&note)
	if nil != err {
		return models.Note{},err
	}
	return note,nil
}
func (n *note)FetchAll(filter bson.M) ([]models.Note,error){
	var notes []models.Note
	col := n.db.Collection(NotesCollection)

	cursor, err := col.Find(context.TODO(),filter)
	if nil != err {
		return nil, err
	}
	for cursor.Next(context.TODO()){
		var note models.Note
		err = cursor.Decode(&note)
		if nil != err {
			return nil, err
		}
		notes = append(notes, note)
	}
	return notes,nil
}
func (n *note)FindOneAndUpdate(filter bson.M, update bson.M) error{
	col := n.db.Collection(NotesCollection)
	res := col.FindOneAndUpdate(context.TODO(),filter,update)
	if res.Err != nil{
		return res.Err()
	}
	return nil
}
func (n *note)DeleteOne(filter bson.M) (int64, error){
	col := n.db.Collection(NotesCollection)
	res, err := col.DeleteOne(context.TODO(),filter)
	if nil != err{
		return 0, err
	}
	return res.DeletedCount,nil
}
func (n *note) DeleteMany(filter bson.M) (int64, error){
	col := n.db.Collection(NotesCollection)
	res, err := col.DeleteMany(context.TODO(),filter)
	if nil != err{
		return 0, err
	}
	return res.DeletedCount,nil
}