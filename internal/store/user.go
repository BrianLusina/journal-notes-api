package store

import (
	"context"
	"github.com/DevAgani/note-app-backend/internal/models"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type userInterface interface {
	InsertOne(user models.User)(primitive.ObjectID,error)
	CheckPresence(filter bson.M) (bool,error)
	DeleteOne(filter bson.M) (int64,error)
	FindOne(filter bson.M) (models.User,error)
	FindOneAndUpdate(filter bson.M, update bson.M) error
}

type user struct {
	db *mongo.Database
}
func (usr *user) InsertOne(user models.User)(primitive.ObjectID,error){
	col := usr.db.Collection(UsersCollection)
	res, err := col.InsertOne(context.TODO(),user)
	if nil != err{
		return primitive.NilObjectID,errors.New("an error occurred while saving a user")
	}
	return res.InsertedID.(primitive.ObjectID),nil
}
func (usr *user) CheckPresence(filter bson.M) (bool,error){
	col := usr.db.Collection(UsersCollection)
	count, err := col.CountDocuments(context.TODO(),filter,options.Count().SetLimit(1))
	return count > 0, err
}
func (usr *user) DeleteOne(filter bson.M) (int64,error){
	col := usr.db.Collection(UsersCollection)
	res, err := col.DeleteOne(context.TODO(),filter)
	if nil != err{
		return 0, err
	}
	return res.DeletedCount,nil
}
func (usr *user) FindOne(filter bson.M) (models.User,error){
	var usrM models.User
	col := usr.db.Collection(UsersCollection)
	res := col.FindOne(context.TODO(),filter)
	if res.Err() != nil{
		return models.User{},res.Err()
	}
	err := res.Decode(&usrM)
	if nil != err{
		return models.User{}, err
	}
	return usrM,nil
}
func (usr *user) FindOneAndUpdate(filter bson.M, update bson.M) error{
	col := usr.db.Collection(UsersCollection)
	res := col.FindOneAndUpdate(context.TODO(),filter,update)
	if res.Err() != nil{
		return res.Err()
	}
	return nil
}