package store

import (
	"context"
	"github.com/DevAgani/note-app-backend/internal/keys"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"time"
)

const (
	UsersCollection = "users"
	NotesCollection = "notes"
)

type StoreInterface interface {
	Connect()
	Disconnect()
	User() userInterface
	Note() noteInterface
}

type store struct {
	db *mongo.Database
}
// Connect creates a connection to our MongoDB
func (store *store) Connect() {
	ctx, _ := context.WithTimeout(context.Background(),10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(keys.GetEnvironmentVariables().MongoUri))
	if nil != err{
		log.Fatal(err.Error())
	}
	err = client.Ping(ctx,readpref.Primary())
	if nil != err{
		log.Fatal("Failed to connect to MongoDB with error :", err.Error())
	}
	store.db = client.Database(keys.GetEnvironmentVariables().MongoDbName)
	collection := store.db.Collection(UsersCollection)
	// Index the email field for faster retrieval
	indexModels := []mongo.IndexModel{
		{
			Keys: bson.M{"email":1},
			Options: options.Index().SetUnique(true),
		},
	}
	collection.Indexes().CreateMany(ctx,indexModels)

	collection = store.db.Collection(NotesCollection)
	indexModels = []mongo.IndexModel{
		{
			Keys: bson.M{"dateCreated" :1 },
			Options: nil,
		},{
			Keys: bson.M{"dateUpdated" :1 },
			Options: nil,
		},
	}
	collection.Indexes().CreateMany(ctx,indexModels)

	log.Println("Connection to MongoDB Database Established Successfully")


}
// Disconnect disconnects out database
func (store *store) Disconnect() {
	store.db.Client().Disconnect(context.TODO())
	log.Println("Successfully disconnected from Database")
}
func (store *store) User() userInterface {
	return &user{
		db: store.db,
	}
}
func (store *store) Note() noteInterface {
	return &note{
		db: store.db,
	}
}
func GetStore() StoreInterface {
	return &store{}
}