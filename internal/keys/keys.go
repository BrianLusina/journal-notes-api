package keys

import (
	"github.com/DevAgani/note-app-backend/internal/models"
	"github.com/joho/godotenv"
	"log"
	"os"
	"sync"
)

var (
	instance models.Keys
	once sync.Once
)


func loadEnvAsString(key string) string {
	godotenv.Load()
	value, ok := os.LookupEnv(key)
	if ok {
		return value
	}
	log.Fatal("Failed to load environment variable : ",key)
	return ""
}

var envVariables = models.Keys{
	MongoUri: loadEnvAsString("MongoUri"),
	MongoDbName: loadEnvAsString("MongoDbName"),
	Port: loadEnvAsString("Port"),
	Secret: loadEnvAsString("Secret"),
}

func GetEnvironmentVariables() models.Keys  {
	once.Do(func() {
		instance = envVariables
	})
	return instance
}
