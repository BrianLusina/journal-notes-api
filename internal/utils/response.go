package utils

import (
	"github.com/DevAgani/note-app-backend/internal/keys"
	"github.com/DevAgani/note-app-backend/internal/models"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)


// RespondWithJSON result a return a JSON response to the client
func RespondWithJSON(c *gin.Context,data interface{})  {
	c.JSON(http.StatusOK,data)
}

// RespondWithSuccess returns a success JSON message.
func RespondWithSuccess(c *gin.Context)  {
	successRes := models.Response{
		Ok:      true,
		Code:    http.StatusOK,
	}
	c.JSON(http.StatusOK,successRes)
}

// RespondWithError returns an error JSON message
func RespondWithError(c *gin.Context, code int, message string)  {
	err := models.Response{
		Ok: false,
		Code: code,
		Message: message,
	}
	c.JSON(err.Code,err)
}

// Authentication Helper method
func GenerateJWT(id string) (int64,string,error)  {
	timestamp := time.Now().Unix()
	expiryTime := timestamp + int64(time.Hour.Seconds() * 4)

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id": id,
		"iat":timestamp,
		"exp": expiryTime,
	})
	signedToken, err := token.SignedString([]byte(keys.GetEnvironmentVariables().Secret))
	if nil != err {
		return 0, "", err
	}
	return expiryTime * 1000, signedToken, nil
}
