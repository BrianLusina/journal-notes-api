package models

import (
	"golang.org/x/crypto/bcrypt"
	"regexp"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)
const (
	EmailRegex = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
)

// User contains information about an author
type User struct {
	ID          primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	FirstName   string             `json:"first_name" bson:"first_name"`
	LastName    string             `json:"last_name" bson:"last_name"`
	Email       string             `json:"email" bson:"email"`
	Password    string             `json:"password,omitempty" bson:"password"`
	IsVerified  bool               `json:"-" bson:"is_verified"`
	Categories  []string           `json:"categories,omitempty" bson:"categories"`
	DateCreated time.Time          `json:"date_created" bson:"date_created"`
	DateUpdated time.Time          `json:"date_updated,omitempty" bson:"date_updated"`
}
// Successful Authenticated Response
type AuthResponse struct {
	User
	JWT string `json:"jwt,omitempty"`
	AccessPass string `json:"access_pass,omitempty"`
	ExpiryTime int64 `json:"expiry_time"`
}

func (usr *User)Validate() bool  {
	if usr.FirstName == "" || usr.LastName == "" || !ValidateEmail(usr.Email){
		return false
	}
	return true
}

//ValidateEmail validates the email address and return true for a valid email and false for an invalid email
func ValidateEmail(email string) bool  {
	return regexp.MustCompile(EmailRegex).MatchString(email)
}

// ValidatePassword verifies the passwords provided by the user
func (usr *User)ValidatePassword(password string) bool  {
	return nil == bcrypt.CompareHashAndPassword([]byte(usr.Password),[]byte(password))
}