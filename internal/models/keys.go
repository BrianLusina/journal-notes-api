package models

// Keys contains all environment variables

type Keys struct {
	MongoUri    string
	MongoDbName string
	Port        string
	Secret		string
}