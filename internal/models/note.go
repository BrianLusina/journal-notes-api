package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Note contains info about a note
type Note struct {
	ID          primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	UserID    primitive.ObjectID 	`json:"user_id" bson:"user_id"`
	User 		*User 				`json:"user,omitempty" bson:"user,omitempty"`
	Title       string             `json:"title" bson:"title"`
	Category    string             `json:"category" bson:"category"`
	Tags        []string           `json:"tags,omitempty" bson:"tags"`
	Content     string             `json:"content" bson:"content"`
	DateCreated time.Time          `json:"date_created" bson:"date_created"`
	DateUpdated time.Time          `json:"date_updated,omitempty" bson:"date_updated"`
}

func (note *Note) IsValid() bool{
	if note.Title == "" || note.Content == ""{
		return false
	}else{
		return true
	}
}
