You will have to use `docker-compose` to run  Database service or any other service as specified in the
[docker-compose file](./docker-compose.yml),

Running docker-compose:

```bash
docker-compose up
```