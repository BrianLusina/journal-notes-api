package main

import "github.com/DevAgani/note-app-backend/internal/server"

func main() {
	server := server.CreateInstance()
	server.Start()
}
