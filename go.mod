module github.com/DevAgani/note-app-backend

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/joho/godotenv v1.3.0
	github.com/pkg/errors v0.8.1
	go.mongodb.org/mongo-driver v1.3.4
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5
)
